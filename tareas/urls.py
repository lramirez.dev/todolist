from django.contrib import admin
from . import views
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('', views.todo, name='inicio'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
