from django.db import models

# Estado de la tarea
ESTADO_TAREA_CHOICES = (
    (0,'Pendiente'),
    (1, 'OK'),
)

class Tarea(models.Model):
    nombreTarea = models.CharField(max_length=100)
    ordenTarea = models.IntegerField(default=0)
    estadoTarea = models.IntegerField(default=0, choices=ESTADO_TAREA_CHOICES)

    def __str__(self):
        return self.nombreTarea