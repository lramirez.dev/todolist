from django.shortcuts import render
from .models import Tarea
from django.conf import settings


def todo(request):
    tareas = Tarea.objects.all().order_by('ordenTarea')
    return render(request, 'tareas/index_vue.html', {'tareas': tareas,})
