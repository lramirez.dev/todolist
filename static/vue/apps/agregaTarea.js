new Vue({
    el: '#app',
    delimiters: ['{$','$}'],
    data: {
        tareas: [],
        tarea: {
            'nombreTarea': '',
            'estadoTarea': 0,
            'ordenTarea': 99999,
        },
    },
    mounted(){
        this.listaTareas();
    },
    beforeUpdate(){
        this.asignaOrden(this.tareas);
    },
    methods: {
        listaTareas(){
            axios.get("http://localhost:8000/api/tareas/")
            .then(respuesta=>this.tareas=respuesta.data.results)
        },

        async creaTarea(){
            await axios.post("http://localhost:8000/api/tareas/", this.tarea)
            .then((result) => {
                console.log(result);
            }).catch(function(error){
                console.log(error);
            });
            this.resetInput();
            this.listaTareas();
        },

        async deleteTarea(id){
            await axios.delete("http://localhost:8000/api/tareas/"+id).then((result) => {
                console.log(result);
            }).catch(function(error){
                console.log(error);
            });
            this.listaTareas();
        },

        async editaTarea(id, nombre, estado){
            if(estado==0){
                estado = 1;
                //document.getElementById("icon-ok-tarea").classList.add("estado-ok");
            }
            else{
                estado = 0;
                //document.getElementById("icon-ok-tarea").classList.toggle("estado-ok");
            }
            
            await axios.put("http://localhost:8000/api/tareas/"+id+"/",{
                "nombreTarea": nombre,
                "estadoTarea": estado
                })
                .then((result) => {
                    console.log(result);
                }).catch(function(error){
                    console.log(error);
                });

            await this.listaTareas();
        },

        async editaTareaOrden(id, nombre, orden){
            
            await axios.put("http://localhost:8000/api/tareas/"+id+"/",{
                "nombreTarea": nombre,
                "ordenTarea": orden
                })
                .then((result) => {
                    console.log(result);
                }).catch(function(error){
                    console.log(error);
                });
        },

        async resetInput() {
            this.tarea.nombreTarea = "";
        },

        async cuentaRegistros(lista){
            return lista.lenght;
        },

        async asignaOrden(lista){
            for(let i = 0; i < lista.length; i++){
                console.log(lista[i].nombreTarea);
                this.editaTareaOrden(lista[i].id, lista[i].nombreTarea, i);
            }
            console.log("actualizado!");
        },
    }
});