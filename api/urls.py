from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'tareas', views.TareaListaJson)

urlpatterns = [
    path('', include(router.urls)),
]
