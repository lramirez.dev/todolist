from rest_framework import serializers

# Llamado a modelos necesarios
from tareas.models import Tarea

class TareaListaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tarea
        fields = ['id', 'nombreTarea', 'estadoTarea', 'ordenTarea' ]

