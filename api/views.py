from django.shortcuts import render
from rest_framework import viewsets, permissions
from .serializers import TareaListaSerializer
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny

from rest_framework.decorators import api_view

# Llamado a modelos necesarios
from tareas.models import Tarea

class TareaListaJson(viewsets.ModelViewSet):
    """
    API endpoint que lista las tareas.
    """
    #permission_classes = [IsAuthenticatedOrReadOnly]
    permission_classes = [AllowAny]
    queryset = Tarea.objects.all().order_by('ordenTarea')
    serializer_class = TareaListaSerializer

